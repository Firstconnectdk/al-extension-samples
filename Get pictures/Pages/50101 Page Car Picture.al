page 50101 CarPicture
{
    Caption = 'Car Picture';
    SourceTable = Car;
    PageType = CardPart;


    layout
    {

        area(Content)
        {
            field(Picture; Picture)
            {
                Caption = 'Picture';
                ShowCaption = false;
                ApplicationArea = all;
            }

        }

    }
    actions
    {
        area(Processing)
        {
            action(Import)
            {
                Caption = 'Import picture from device';
                Image = Picture;
                ApplicationArea = All;
                //Enabled = EnableImportImage;
                trigger OnAction();
                var

                    OverrideImageQst: Text[250];
                    FromFileName: Text;
                    PicInStream: InStream;
                    picOutStream: OutStream;
                    imageID: Guid;


                begin
                    if rec.Model = '' then
                        exit;
                    OverrideImageQst := 'The existing picture will be replaced. Do you want to continue?';
                    IF Picture.Count > 0 then
                        IF NOT CONFIRM(OverrideImageQst) THEN
                            EXIT;
                    if UploadIntoStream('Import', '', 'All Files (*.*)|*.*', FromFileName, PicInStream) then begin
                        Clear(Picture);
                        Picture.ImportStream(PicInStream, 'Care Label symbol ' + FORMAT(rec.Picture));
                        IF NOT INSERT(TRUE) THEN
                            MODIFY(TRUE);

                    end;
                end;
            }
            

            
        }
    }
}