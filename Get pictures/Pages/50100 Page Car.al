page 50100 "Car"
{
    Caption = 'Car';
    PageType = Card;
    SourceTable = Car;
    UsageCategory = Documents;
    ApplicationArea = All;
    DeleteAllowed = true;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Model; Model)
                {
                    Caption = 'Model';
                    ApplicationArea = All;
                    ShowMandatory = true;
                }
            }
        }
        area(FactBoxes)
        {
            part(Image; CarPicture)
            {
                Caption = 'Car Picture';
                UpdatePropagation = Both;
                ApplicationArea = All;
                SubPageLink = Model = field (Model);
            }
        }

    }
}
