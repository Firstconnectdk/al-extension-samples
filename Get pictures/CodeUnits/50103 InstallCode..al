codeunit 50103 InstallCode
{
    // Set the codeunit to be an install codeunit. 
    Subtype = Install;


    // This trigger includes code for company-related operations. 
    trigger OnInstallAppPerCompany();
    var
        MyCar : Record Car;
    begin
        if MyCar.IsEmpty() then begin
            InsertDefaulCars();
        end;
        
    end;


    //Seeding some demo data into into Car table 
    local procedure InsertDefaulCars()

    begin
        InsertCar('Volvo XC40', 'https://fcfashion.blob.core.windows.net/test/volvo-xc40.jpg');
        InsertCar('Seat Alteca SUV', 'https://fcfashion.blob.core.windows.net/test/seat.jpg');
        InsertCar('Audi Q3', 'https://fcfashion.blob.core.windows.net/test/Audi.jpg');
    end;


    // Insert car into table
    local procedure InsertCar(model: text[100]; pictureurl : text[250])
    var
        carRS: Record Car;
    begin
        carRS.Init();
        carRS.Model := model;
        GetPicture(carRS, pictureurl);
        carRS.Insert();

    end;

    // Get Picture from Azure Blob

 procedure GetPicture(var rs : Record Car; url : text)  
   var
       HttpClient : HttpClient;
       MyHttpResponseMessage : HttpResponseMessage;
       MyinStream : InStream;
       
   begin
       if HttpClient.Get(url, MyHttpResponseMessage) then begin
           MyHttpResponseMessage.Content.ReadAs(MyinStream);
           rs.Picture.ImportStream(MyinStream, rs.model);
       end;   
    end;

}







