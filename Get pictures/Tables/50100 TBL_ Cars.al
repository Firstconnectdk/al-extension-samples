table 50100 Car
{
    Caption = 'Car';
    DataPerCompany = true;

    fields
    {
        field(1;Model;Code[50])
        {
            Caption = 'Care Label Symbol Id';
            NotBlank = true;
        }
        field(2;Picture;MediaSet)
        {
              Caption = 'Car Picture'; 
              
        }
    }
    keys
    {
        key(PrimaryKey;Model)
        {
            Clustered=TRUE;
        }
    }
    
}